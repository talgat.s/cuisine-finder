package main

import (
	"cuisine-finder/server"
)

func main() {
	server.Start()
}
