module cuisine-finder

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.1.1
)
