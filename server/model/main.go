package model

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
)

const (
	RESTAURANTS = "restaurants"
)

type Table interface {
	Create()
	Drop()
}

type columns map[string]string

type dbConn func(db *sql.DB)

func conn(fn dbConn) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatalf("Cannot connect to db ::", err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatalf("Ping db error ::", err)
	}
	fn(db)
}

func Setup() {
	fmt.Println(Restaurants["RESTAURANTS_ID"])
}
