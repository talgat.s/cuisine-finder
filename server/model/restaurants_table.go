package model

import (
	"database/sql"
	"fmt"
	"log"
)

const (
	RESTAURANTS_ID           = "id"
	RESTAURANTS_COMPANY_NAME = "company_name"
	RESTAURANTS_REG_NUM      = "reg_num"
	RESTAURANTS_START_DATE   = "start_date"
	RESTAURANTS_END_DATE     = "end_date"
	RESTAURANTS_BRAND_NAME   = "brand_name"
	RESTAURANTS_LOGO         = "logo"
	RESTAURANTS_ACTIVITY     = "activity"
	RESTAURANTS_DESCRIPTION  = "description"
	RESTAURANTS_ADDRESS      = "address"
	RESTAURANTS_EMAIL        = "email"
	RESTAURANTS_WEB_SITE     = "site"
	RESTAURANTS_TEL          = "tel"
	RESTAURANTS_INSPECTOR    = "inspector"
	RESTAURANTS_CREATED      = "created"
	RESTAURANTS_UPDATED      = "updated"
)

var Restaurants columns = columns{"RESTAURANTS_ID": "id"}

func (u Restaurants) Create(db *sql.DB) {
	_, err := db.Exec(
		fmt.Sprintf(`
			CREATE EXTENSION IF NOT EXISTS "pgcrypto";
			CREATE TABLE IF NOT EXISTS %[1]s(
				%[2]s serial PRIMARY KEY,															-- RESTAURANTS_ID
				%[3]s text NOT NULL CHECK (char_length(%[3]s) >= 3), 	-- RESTAURANTS_COMPANY_NAME
				%[4]s text UNIQUE NOT NULL, 													-- RESTAURANTS_REG_NUM
				%[5]s timestamptz NOT NULL, 													-- RESTAURANTS_START_DATE
				%[6]s timestamptz NOT NULL, 													-- RESTAURANTS_END_DATE
				%[7]s text UNIQUE NOT NULL, 													-- RESTAURANTS_BRAND_NAME
				%[8]s text UNIQUE NOT NULL, 													-- RESTAURANTS_LOGO
				%[9]s text, 																					-- RESTAURANTS_ACTIVITY
				%[10]s text, 																					-- RESTAURANTS_DESCRIPTION
				%[11]s text, 																					-- RESTAURANTS_ADDRESS
				%[12]s text, 																					-- RESTAURANTS_EMAIL
				%[13]s text, 																					-- RESTAURANTS_WEB_SITE
				%[14]s text, 																					-- RESTAURANTS_TEL
				%[15]s text, 																					-- RESTAURANTS_INSPECTOR
				%[16]s timestamptz NOT NULL DEFAULT NOW(), 						-- RESTAURANTS_CREATED
				%[17]s timestamptz NOT NULL DEFAULT NOW() 						-- RESTAURANTS_UPDATED
			);
		`,
			RESTAURANTS,
			RESTAURANTS_ID,
			RESTAURANTS_COMPANY_NAME,
			RESTAURANTS_REG_NUM,
			RESTAURANTS_START_DATE,
			RESTAURANTS_END_DATE,
			RESTAURANTS_BRAND_NAME,
			RESTAURANTS_LOGO,
			RESTAURANTS_ACTIVITY,
			RESTAURANTS_DESCRIPTION,
			RESTAURANTS_ADDRESS,
			RESTAURANTS_EMAIL,
			RESTAURANTS_WEB_SITE,
			RESTAURANTS_TEL,
			RESTAURANTS_INSPECTOR,
			RESTAURANTS_CREATED,
			RESTAURANTS_UPDATED,
		),
	)
	if err != nil {
		log.Fatalf("Create %s table error :: ", RESTAURANTS, err)
	}
}

func (u Restaurants) Drop(db *sql.DB) {
	_, err := db.Exec(
		fmt.Sprintf(`
			DROP TABLE IF EXISTS %[1]s;
		`,
			RESTAURANTS,
		),
	)
	if err != nil {
		log.Fatalf("DROP %s table error :: ", RESTAURANTS, err)
	}
}
