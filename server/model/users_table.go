package model

import (
	"database/sql"
	"fmt"
	"log"
)

const (
	USERS_ID       = "id"
	USERS_USERNAME = "username"
	USERS_EMAIL    = "email"
	USERS_PASSWORD = "password"
	USERS_CREATED  = "created"
	USERS_UPDATED  = "updated"
)

type User struct{}

func (u User) Create(db *sql.DB) {
	_, err := db.Exec(
		fmt.Sprintf(`
			CREATE EXTENSION IF NOT EXISTS "pgcrypto";
			CREATE TABLE IF NOT EXISTS %[1]s(
				%[2]s text PRIMARY KEY UNIQUE DEFAULT CONCAT('user-', gen_random_uuid()),
				%[3]s text UNIQUE NOT NULL
					CHECK (char_length(%[3]s) >= 3),
				%[4]s text UNIQUE NOT NULL
					CHECK (%[4]s ~* '^[a-z0-9_%+.-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
				%[5]s text,
				%[6]s timestamptz NOT NULL DEFAULT NOW(),
				%[7]s timestamptz NOT NULL DEFAULT NOW()
			);

			CREATE OR REPLACE FUNCTION encrypt_password_trigger() RETURNS TRIGGER AS $$
				BEGIN
					IF
						NEW.%[5]s ~ '^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[$!@_ %#~^*$&()?\-+=\d]).*$'
					THEN
						NEW.%[5]s := crypt(NEW.%[5]s, gen_salt('bf', 8));
					ELSE
						RAISE check_violation
						USING
							TABLE = '%[1]s',
							COLUMN = '%[5]s',
							DATATYPE = 'text',
							CONSTRAINT = '%[1]s_%[5]s_check',
							MESSAGE = 'new row for relation "%[1]s" violates check constraint "%[1]s_%[5]s_check"';
					END IF;
					RETURN NEW;
				END;
			$$ LANGUAGE plpgsql;
			DROP TRIGGER IF EXISTS encrypt_password ON %[1]s;
			CREATE TRIGGER encrypt_password BEFORE INSERT OR UPDATE ON %[1]s
			FOR EACH ROW
			EXECUTE PROCEDURE encrypt_password_trigger();
		`,
			// USERS,
			USERS_ID,
			USERS_USERNAME,
			USERS_EMAIL,
			USERS_PASSWORD,
			USERS_CREATED,
			USERS_UPDATED,
		),
	)
	if err != nil {
		log.Fatal("Create user error :: ", err)
	}
}
