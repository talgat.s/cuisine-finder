package server

import (
	"log"
	"os"

	"cuisine-finder/server/config"
	"cuisine-finder/server/model"
	"cuisine-finder/server/routes"
)

func Start() {
	config.SetupGin()
	config.LoadVars()

	model.Setup()
	// u := model.User{}
	// u.Create()

	router := routes.Setup()
	err := router.Run(":" + os.Getenv("PORT"))
	if err != nil {
		log.Fatal("error starting http server ::", err)
		return
	}
}
