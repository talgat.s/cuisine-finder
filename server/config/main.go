package config

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

const (
	ENV_KEY = "APP_ENV"
)

var ENV string = os.Getenv(ENV_KEY)

func SetupGin() {
	switch ENV {
	case "production":
		gin.SetMode(gin.ReleaseMode)
	default:
		gin.SetMode(gin.DebugMode)
	}
}

func LoadVars() {
	switch ENV {
	case "production":
		err := godotenv.Load(".env.production")
		if err != nil {
			log.Println("Error loading .env.production file ::", err)
		}
	default:
		err := godotenv.Load(".env.development")
		if err != nil {
			log.Println("Error loading .env.development file ::", err)
		}
	}
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file ::", err)
	}
}
