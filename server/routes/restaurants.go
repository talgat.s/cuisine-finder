package routes

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Restaurant struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
type Restaurants []Restaurant

// TODO remove it after setting model
var restaurants = Restaurants{
	Restaurant{Id: 1, Name: "Restaurant 1", Description: "Description 1"},
	Restaurant{Id: 2, Name: "Restaurant 2", Description: "Description 2"},
}

func setupRestaurantsRoutes(r *gin.Engine) {
	r.GET("/restaurants", func(c *gin.Context) {
		c.JSON(http.StatusOK, restaurants)
	})

	r.GET("/restaurants/:id", func(c *gin.Context) {
		id, err := strconv.Atoi(c.Param("id"))

		if err != nil {
			c.Request.URL.Path = "/not-found"
			r.HandleContext(c)
		}

		for _, restaurant := range restaurants {
			if restaurant.Id == id {
				c.JSON(http.StatusOK, restaurant)
				break
			}
		}
	})
}
