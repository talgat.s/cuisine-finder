package routes

import (
	"github.com/gin-gonic/gin"
)

func Setup() *gin.Engine {
	router := gin.Default()

	setupRestaurantsRoutes(router)

	return router
}
